#! /bin/bash
#script che legge l'indirizzo ip dal sito "http://myip.dk/"
downloaddir=~/.conky/
ipnum=[0-9][0-9][0-9]\.[0-9][0-9][0-9]\.[0-9][0-9][0-9]\.[0-9][0-9][0-9]
if [ ! -d $downloaddir ]
then
	mkdir $downloaddir
	cd $downloaddir
else
	cd $downloaddir
	rm ./*
fi
#wget -q http://myip.dk/
wget -q http://whatismyip.org/
line=$(grep -o '[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}' index.html)
echo $line